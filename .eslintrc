{
    "parser": "babel-eslint",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 12
    },
    "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:react/recommended",
        "plugin:react-hooks/recommended",
        "plugin:jsx-a11y/recommended",
        "plugin:prettier/recommended"
    ],
    "plugins": ["prettier", "react", "react-hooks", "jsx-a11y", "simple-import-sort", "import"],
    "env": {
        "browser": true,
        "es6": true,
        "jest": true,
        "node": true
    },
    "settings": {
        "react": {
            "version": "detect"
        },
        "import/resolver": {
            "node": {
                "moduleDirectory": ["node_modules", "src"]
            }
        }
    },
    "rules": {
        "prettier/prettier": "error",
        "consistent-return": "off",
        "no-underscore-dangle": "off",
        "no-param-reassign": "off",
        // "no-console": ["warn", { "allow": ["warn", "error", "info"] }],

        // Suppress errors for missing 'import React' in files
        "react/react-in-jsx-scope": "off",
        // Use TS to handle prop-types instead
        "react/prop-types": "off",

        // "react/jsx-props-no-spreading": "off",
        "react-hooks/exhaustive-deps": "warn",

        "@typescript-eslint/no-explicit-any": "warn",
        "@typescript-eslint/no-unused-vars": "error",
        "@typescript-eslint/ban-ts-comment": "warn",

        "simple-import-sort/imports": "error",

        "simple-import-sort/exports": "error",

        "import/imports-first": 0,
        // Makes sure all imports are at the top of the file
        "import/newline-after-import": 0,
        // Makes sure there’s a newline after the imports
        // Merges import statements of the same file
        "import/no-duplicates": 0
    },
    "ignorePatterns": ["node_modules", "**/*.js"]
}

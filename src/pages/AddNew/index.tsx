import React from 'react';

import { AddNewLayout } from '../../layouts';

const AddNewPage: React.FC = () => {
    return <AddNewLayout />;
};

export default AddNewPage;

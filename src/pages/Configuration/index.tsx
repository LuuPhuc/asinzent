import React from 'react';

import { ConfigurationLayout } from '../../layouts';

const ConfigurationPage: React.FC = () => {
    return <ConfigurationLayout />;
};

export default ConfigurationPage;

/**
 * App name
 */
export const APP_NAME = process.env.REACT_APP_NAME || 'Asinzen test';

/**
 * App version
 */
export const APP_VERSION = process.env.REACT_APP_VERSION || 'v1.0.0';

/**
 * API URL
 */
export const API_URL = process.env.REACT_APP_API_URL || 'http://127.0.0.1:5000/v1';

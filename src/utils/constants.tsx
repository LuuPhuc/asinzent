export const PATH = {
    CONFIGURATION: '/configuration',
    ADD_NEW: '/add-new',
};

export const PAGES = {
    NAME: {
        CONFIGURATION: 'configuration',
        ADD_NEW: 'add-new',
    },
};

export const HeaderConfiguration = [
    {
        key: 'google-spreadsheet',
        dataIndex: 'googleSpreadsheet',
        title: 'Google spreadsheet',
    },
    {
        key: 'google-sheet',
        dataIndex: 'sheet',
        title: 'Google sheet',
    },
    {
        key: 'header-row',
        dataIndex: 'headerRow',
        title: 'Header row',
    },
    {
        key: 'action',
        dataIndex: 'action',
        title: 'Action',
    },
];

export const googleSpreadsheet = [
    {
        required: true,
        message: 'Please select your google spreadsheet',
    },
];

export const sheet = [
    {
        required: true,
        message: 'Please select your sheet',
    },
];

export const rowEnd = [
    {
        pattern: /^[a-zA-Z]+\d+$/g,
        message: 'Row invalid',
    },
];

export const rowStart = [
    {
        pattern: /^[a-zA-Z]+\d+$/g,
        message: 'Row invalid',
    },
];

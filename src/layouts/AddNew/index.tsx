import './styles.less';

import { Button, Form, Typography } from 'antd';
import { upperCase } from 'lodash';
import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';

import { CheckBox, Input, Section, SectionTitle, Select } from '../../components/base';
import { SheetContext } from '../../contexts';
import { Spreadsheet } from './addnew';
import { googleSpreadsheet, rowEnd, rowStart, sheet } from './validation';

const { Text } = Typography;

interface IAddNewLayout {}

interface IValues {
    google_spreadsheet?: string;
    sheet?: string;
    row_start?: string;
    row_end?: string;
}

const AddNewLayout: React.FC<IAddNewLayout> = () => {
    const [check, setCheck] = useState();
    const router = useHistory();
    const { sheets, setSheets } = useContext(SheetContext);

    const [clearText, setClearText] = useState('');
    const [form] = Form.useForm();

    const onCheck = (e: any) => {
        setClearText('');
        setCheck(e.target.checked);
    };

    const onFinish = (values: IValues) => {
        const parseValue = {
            key: values.google_spreadsheet,
            googleSpreadsheet: values.google_spreadsheet,
            sheet: values?.sheet,
            headerRow: upperCase(`${values?.row_start} : ${values?.row_end}`),
            action: 'Edit Delete',
        };

        console.log('Received values of form: ', parseValue);
        if (setSheets) setSheets([...sheets, parseValue]);
        router.push('/configuration');
    };

    return (
        <Section id="add-new">
            <SectionTitle>Add New Google Sheet</SectionTitle>
            <Form form={form} name="add-new" onFinish={onFinish}>
                <div className="addNewWrapper">
                    <div className="selectSheetContainer">
                        {/* Google spreadsheet */}
                        <Form.Item
                            name="google_spreadsheet"
                            rules={googleSpreadsheet}
                            className="itemGoogleSpreadsheet"
                        >
                            <Select
                                onChange={(value: any) => value}
                                id="google_spreadsheet"
                                showSearch={true}
                                placeholder="Select your Google Spreadsheet"
                                options={Spreadsheet}
                                title="Step 1: Select your Google spreadsheet and sheet"
                            />
                        </Form.Item>

                        {/* Sheet */}
                        <Form.Item name="sheet" rules={sheet} className="itemSelectSheet">
                            <Select
                                onChange={(value: any) => value}
                                id="sheet"
                                showSearch={true}
                                placeholder="Select your sheet"
                                options={Spreadsheet}
                            />
                        </Form.Item>
                    </div>

                    <div className="checkboxContainer">
                        <CheckBox
                            title="Step 2: Define header row"
                            label="Yes, my sheet contains header row"
                            onChange={onCheck}
                        />
                    </div>

                    <div className="defineHeaderRowContainer">
                        <Text className="text">My header row start from</Text>

                        {/* Row start */}
                        <Form.Item name="row_start" rules={rowStart} className="itemInputHeaderRow">
                            <Input placeholder="From" disabled={!check} value={clearText} />
                        </Form.Item>

                        <Text className="text">and end on cell</Text>

                        {/* Row end */}
                        <Form.Item name="row_end" rules={rowEnd} className="itemInputHeaderRow">
                            <Input placeholder="To" disabled={!check} value={clearText} />
                        </Form.Item>
                    </div>

                    <div className="forExampleWrapper">
                        <Text className="forExample">For example: from cell A1 to cell C1</Text>
                    </div>

                    <Button type="primary" className="saveBtn" htmlType="submit">
                        SAVE
                    </Button>
                </div>
            </Form>
        </Section>
    );
};
export default AddNewLayout;

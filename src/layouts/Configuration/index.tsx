import './styles.less';

import { Button, Form, Input, Modal, Typography } from 'antd';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import { Section, SectionTitle, Table } from '../../components/base';
import { SheetContext } from '../../contexts';
import { HeaderConfiguration } from '../../utils/constants';
import { validateMessages } from './validation';
interface IConfigurationLayout {
    email?: string;
}

const { Text } = Typography;

const ConfigurationLayout: React.FC<IConfigurationLayout> = () => {
    const { sheets } = useContext(SheetContext);
    const [connected, setConnected] = useState(false);
    const [visible, setVisible] = useState(false);
    const [email, setEmail] = useState('');
    const [form] = Form.useForm();

    const router = useHistory();

    const handleCancel = () => {
        setVisible(false);
    };

    const onEnterEmail = (value: any) => {
        setEmail(value?.email);

        setVisible(false);
    };

    const onClick = () => {
        if (!email) {
            setVisible(true);
        } else {
            router.push('/add-new');
        }
    };

    const onConnect = () => {
        // setEmail('luuphuc6297@gmail.com');
        if (!email) {
            setVisible(true);
        }
        setConnected(true);
    };

    const onDisconnect = () => {
        setEmail('');
        setConnected(false);
    };

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    useEffect(() => {}, [sheets]);

    return (
        <Section id="configuration">
            <div className="configWrapper">
                <SectionTitle>Google Sheet Configuration</SectionTitle>

                <div className="configContainer">
                    <Text className="caption">Google status: </Text>

                    <Text className="status">{`Connected with ${email || `<<email>>`}`}</Text>
                    {connected ? (
                        <Button onClick={onDisconnect} className="disconnect">
                            Disconnect
                        </Button>
                    ) : (
                        <Button onClick={onConnect} className="connect">
                            Connect
                        </Button>
                    )}
                </div>

                <div className="tableWrapper">
                    <Table columns={HeaderConfiguration} dataSource={sheets} />
                </div>

                <div className="addNewBtnWrapper">
                    <Button className="addNew" onClick={onClick}>
                        Add New Google Sheet
                    </Button>
                    <Modal title="You already not connect" visible={visible} onOk={form.submit} onCancel={handleCancel}>
                        <Form
                            form={form}
                            name="enter-email"
                            validateMessages={validateMessages}
                            onFinish={onEnterEmail}
                        >
                            <Form.Item name="email" rules={[{ type: 'email' }]}>
                                <Input placeholder="Enter your email" />
                            </Form.Item>
                        </Form>
                    </Modal>
                </div>
            </div>
        </Section>
    );
};

export default ConfigurationLayout;

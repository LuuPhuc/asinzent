import React from 'react';

// Constant
import { PAGES, PATH } from '../utils/constants';

// Pages
const ConfigurationPage = React.lazy(() => import('../pages/Configuration'));
const AddNewPage = React.lazy(() => import('../pages/AddNew'));

export const publicPages = [
    {
        path: PATH.CONFIGURATION,
        name: PAGES.NAME.CONFIGURATION,
        component: ConfigurationPage,
        auth: false,
        exact: true,
    },
    {
        path: PATH.ADD_NEW,
        name: PAGES.NAME.CONFIGURATION,
        component: AddNewPage,
        auth: false,
        exact: true,
    },
];

export const privatePages = [];

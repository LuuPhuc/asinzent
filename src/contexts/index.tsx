import React, { Dispatch, SetStateAction } from 'react';

export interface IInit {
    key: string | number | undefined;
    googleSpreadsheet: string | number | undefined;
    sheet: string | number | undefined;
    headerRow?: string | number;
    action?: string | number;
}

interface SheetContextProps {
    sheets: IInit[];
    setSheets?: Dispatch<SetStateAction<IInit[]>>;
}

const defaultValue: SheetContextProps = {
    sheets: [],
};

export const SheetContext = React.createContext<SheetContextProps>(defaultValue);

export const SheetProvider: React.FC<React.ReactNode> = ({ children }) => {
    const [sheets, setSheets] = React.useState<IInit[]>(defaultValue.sheets);

    return <SheetContext.Provider value={{ sheets, setSheets }}>{children}</SheetContext.Provider>;
};

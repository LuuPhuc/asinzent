/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import './App.less';

import React, { Suspense } from 'react';
import { Helmet } from 'react-helmet';
import { Route, Switch } from 'react-router-dom';

import { APP_NAME } from './configs/constants';
import { publicPages } from './routers/routes';

const publicRoutes = publicPages.map(({ path, ...props }) => <Route key={path} path={path} {...props} />);

function App() {
    return (
        <div className="App">
            <Helmet defaultTitle={APP_NAME} titleTemplate={`${APP_NAME} | %s`} />
            <Suspense fallback={<div>Loading...</div>}>
                <Switch>{publicRoutes}</Switch>
            </Suspense>
        </div>
    );
}

export default App;

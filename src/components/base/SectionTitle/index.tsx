import './styles.less';

import { Typography } from 'antd';
import React from 'react';

interface ISectionTitle {
    children?: string;
}

const { Title } = Typography;

const SectionTitle: React.FC<ISectionTitle> = ({ children }) => {
    return (
        <Title level={1} className="title">
            {children}
        </Title>
    );
};

export default SectionTitle;

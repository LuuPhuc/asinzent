import './styles.less';

import { Checkbox, Typography } from 'antd';
import React from 'react';
const { Text } = Typography;

interface ICheckBox {
    id?: string;
    label?: string;
    title?: string;
    onChange?: (e: unknown) => void;
}

const CustomCheckBox: React.FC<ICheckBox> = ({ id, label, onChange, title, ...props }) => {
    return (
        <div className="checkboxWrapper">
            <Text className="checkboxTitle" strong>
                {title}
            </Text>
            <Checkbox id={id} onChange={onChange} className="label" {...props}>
                {label}
            </Checkbox>
        </div>
    );
};

export default CustomCheckBox;

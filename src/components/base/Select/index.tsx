import './styles.less';

import { Select, Typography } from 'antd';
import clsx from 'clsx';
import React from 'react';

const { Text } = Typography;

declare type Key = string | number;

interface IOptions {
    key?: string;
    value: Key;
    label?: string;
}

interface ISelect {
    showSearch?: boolean;
    placeholder?: string;
    options: IOptions[];
    error?: string;
    title?: string;
    className?: string;
    id?: string;
    onChange?: (value: any) => void;
}

const CustomSelect: React.FC<ISelect> = ({ id, onChange, placeholder, options, title, className }) => {
    return (
        <div className={clsx('selectWrapper', className)}>
            <Text strong className="titleSelect">
                {title}
            </Text>
            <Select
                id={id}
                showSearch
                className="select"
                placeholder={placeholder}
                filterOption={(input, option) => option?.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                onChange={onChange}
            >
                {options.map((item) => (
                    <Select.Option key={item?.key} value={item?.value}>
                        {item?.label}
                    </Select.Option>
                ))}
            </Select>
        </div>
    );
};

export default CustomSelect;

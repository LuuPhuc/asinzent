import './styles.less';

import React from 'react';

interface ISection {
    id?: string;
}

const Section: React.FC<ISection> = ({ children, id }) => {
    return (
        <section id={id} className="section">
            {children}
        </section>
    );
};

export default Section;

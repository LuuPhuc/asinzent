import { Table } from 'antd';
import React from 'react';

import { IInit } from '../../../contexts';

interface IColumn {
    title?: string;
    dataIndex?: string;
    key?: string;
}

interface ICustomTable {
    columns: IColumn[];
    dataSource: IInit[];
}

const CustomTable: React.FC<ICustomTable> = ({ columns, dataSource }) => {
    return <Table className="table" columns={columns} dataSource={dataSource || []} size="small" />;
};

export default CustomTable;

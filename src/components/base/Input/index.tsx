import { Input } from 'antd';
import clsx from 'clsx';
import React from 'react';

// const { Text } = Typography;

interface IInput {
    id?: string;
    value?: string | number;
    placeholder?: string;
    error?: string;
    className?: string;
    disabled?: boolean;
}
const CustomInput: React.FC<IInput> = ({ disabled, placeholder, className, id, value, ...props }) => {
    return (
        <div className={clsx('inputWrapper', className)}>
            <Input placeholder={placeholder} className="input" id={id} value={value} {...props} disabled={disabled} />
        </div>
    );
};

export default CustomInput;
